# scrape-zaim.js
scrape-zaim.js is a GAS code for scraping Zaim top pages after login.

If you want to know more, move to my [blog](https://tetsuri-ariyama.gitlab.io/ja/posts/scrape-zaim/) (Japanease language only).

## Getting started
Copy a body of scrape-zaim.js and paste it into your GAS project.

## Usage
Run scrapeZaim() after setting your e-mail and password. Return value is .html body(text type) of Zaim top page.

## Contributing
When you find a bug, please contact [me](https://tetsuri-ariyama.gitlab.io/contact/). Thank you!

## License
[MIT](https://choosealicense.com/licenses/mit/)
