USER_EMAIL = 'Zaimに登録したメールアドレス';
USER_PASSWORD = 'Zaimに登録したパスワード';

function scrapeZaim() {
  // URLs
  var url;
  var nextUrl;
  var urlNet = 'https://auth.zaim.net/';
  var urlHttps = 'https://auth.zaim.net/users/login/https%3A%2F%2Fzaim.net%2Fhome';
  var urlHome = 'https://zaim.net/home';
  var urlNew = 'https://zaim.net/user_session/new';
  var urlAuth = 'https://auth.zaim.net/users/auth';
  var urlCallback = 'https://zaim.net/user_session/callback';

  var options;  // options of UrlFetchApp.fetch()
  var response;  // return value of UrlFetchApp.fetch()
  var cookies;  // String type
  var cookieZ;  // String type
  var cookieY;  // String type
  var headers;  // associative array

  // ------- https://auth.zaim.net/
  url = urlNet;

  var payload = {
    'data[User][email]': USER_EMAIL,
    'data[User][password]': USER_PASSWORD
  };

  options = {
    'method' : 'POST',
    'payload' : payload,
    'followRedirects' : false
  };

  // POST request
  Logger.log(`--- POST request to ${url} .`);
  response = UrlFetchApp.fetch(url, options);

  // Get cookie from response header
  cookies = response.getHeaders()['Set-Cookie'];
  cookieZ = cookies;

  // Store cookie to headers
  headers = {'Cookie' : cookies};

  // Get location(next url)
  nextUrl = response.getHeaders()['Location'];

  // ------- https://auth.zaim.net/users/login/https%3A%2F%2Fzaim.net%2Fhome
  // Only get location url here.
  if(urlHttps !== nextUrl){
    console.error(`Worng URL: ${nextUrl} .`);
  }

  url = nextUrl;

  options = {
    'method' : 'GET',
    'headers' : headers,
    'followRedirects': false
  };

  // GET request
  Logger.log(`--- GET request to ${url} .`);
  response = UrlFetchApp.fetch(url, options);

  // Get location(next url)
  nextUrl = response.getHeaders()['Location'];

  // ------- https://zaim.net/home
  if(urlHome !== nextUrl){
    console.error(`Worng URL: ${nextUrl} .`);
  }

  url = nextUrl;

  options = {
    'method' : 'GET',
    'headers' : headers,  // No update
    'followRedirects': false
  };

  // GET request
  Logger.log(`--- GET request to ${url} .`);
  response = UrlFetchApp.fetch(url, options);

  // Get cookie from response header
  cookies = response.getHeaders()['Set-Cookie'];
  cookieY = cookies;

  // Store cookie to headers
  cookies = cookieZ + '; ' + cookieY;  // use two cookies
  headers = {'Cookie' : cookies};

  // Get location(next url)
  nextUrl = response.getHeaders()['Location'];

  // ------- https://zaim.net/user_session/new
  if(urlNew !== nextUrl){
    console.error(`Worng URL: ${nextUrl} .`);
  }

  url = nextUrl;

  options = {
    'method' : 'GET',
    'headers' : headers,
    'followRedirects': false
  };

  // GET request
  Logger.log(`--- GET request to ${url} .`);
  response = UrlFetchApp.fetch(url, options);

  // Get cookie from response header
  cookies = response.getHeaders()['Set-Cookie'];
  cookieY = cookies;

  // Store cookie to headers
  cookies = cookieZ + '; ' + cookieY;  // use two cookies
  headers = {'Cookie' : cookies};
  //Logger.log(headers);

  // Get location(next url)
  nextUrl = response.getHeaders()['Location'];

  // ------- https://auth.zaim.net/users/auth
 if(urlAuth !== nextUrl.slice(0, urlAuth.length)){
    console.error(`Worng URL: ${nextUrl} .`);
  }

  url = nextUrl;  // with oauth_token

  options = {
    'method' : 'GET',
    'headers' : headers,
    'followRedirects': false,
  };

  // GET request
  Logger.log(`--- GET request to ${url} .`)
  response = UrlFetchApp.fetch(url, options);

  // Get target text for next location(with oauth_token and oauth_verifier)
  // ex) location.href = "https:\/\/zaim.net\/user_session\/callback?oauth_token=hoge...&oauth_verifier=fuga.."; }, 500);
  var content = response.getContentText('UTF-8');
  //Logger.log(content);

  // Get only https
  var fromText = ' location.href = ';
  var toText = '"; }, 500)';
  var targetText = Parser.data(content).from([fromText]).to([toText]).iterate()[0];

  // remove fromText and backslash
  targetText = targetText.slice(fromText.length);
  nextUrl = targetText.replace(/\\/g, '')

  // take a sleep
  Utilities.sleep(500);

  // ------- https://zaim.net/user_session/callback
 if(urlCallback !== nextUrl.slice(0, urlCallback.length)){
    console.error(`Worng URL: ${nextUrl} .`);
  }

  url = nextUrl;  // With oauth_token and oauth_verifier

  options = {
    'method' : 'GET',
    'headers' : headers,  // No update
    'followRedirects': true, // Enable redirect
  };

  // GET request
  Logger.log(`--- GET request to ${url} .`)
  response = UrlFetchApp.fetch(url, options);
  //Logger.log(response.getContentText('UTF-8'));

  return response.getContentText('UTF-8');
}
